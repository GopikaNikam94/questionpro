package questionpro.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import questionpro.dao.DAOManager;
import questionpro.model.Story;
import questionpro.util.ErrorInfo;
import questionpro.util.JSONUtil;
import questionpro.util.RequestFormattedIncorrectlyException;
import questionpro.util.WebHeader;
import questionpro.util.WebRequest;
import questionpro.util.WebResponse;

public class DefaultStoryService implements StoryService {

	private final static Logger logger = LoggerFactory.getLogger(DefaultStoryService.class);

	private DAOManager daoManager;

	@Override
	public WebResponse createStory(WebRequest webRequest) {
		validateRequest();
		Story story = JSONUtil.convertBody(webRequest.getBody(), Story.class);
		logger.debug("Story : {}", story);

		boolean dbQueryResult = daoManager.getStoryDAO().saveStory(story);
		checkQueryResult(dbQueryResult);

		WebResponse webResponse = createDefaultResponse(webRequest.getHeader());
		webResponse.setBody("New Story Created!");
		return webResponse;
	}

	@Override
	public WebResponse fetchStories() {
		List<Story> storyList = daoManager.getStoryDAO().fetchStories();
		logger.trace("Story List: {}", storyList);

		if (storyList.isEmpty()) {
			return getErrorResponse((short) 404, "Empty Story List");
		}

		WebResponse webResponse = createDefaultResponse();
		webResponse.setBody(storyList);
		return webResponse;
	}

	@Override
	public WebResponse fetchAllStories() {
		List<Story> storyList = daoManager.getStoryDAO().fetchAllStories();
		logger.trace("Story List: {}", storyList);

		if (storyList.isEmpty()) {
			return getErrorResponse((short) 404, "Empty All Story List");
		}

		WebResponse webResponse = createDefaultResponse();
		webResponse.setBody(storyList);
		return webResponse;
	}
	
	@Override
	public WebResponse fetchCommentStories() {
		List<Story> storyList = daoManager.getStoryDAO().fetchCommentStories();
		logger.trace("Story List: {}", storyList);

		if (storyList.isEmpty()) {
			return getErrorResponse((short) 404, "Empty Comment Story List");
		}

		WebResponse webResponse = createDefaultResponse();
		webResponse.setBody(storyList);
		return webResponse;
	}

	private void checkQueryResult(boolean dbQueryResult) {
		if (dbQueryResult) {
			throw new RequestFormattedIncorrectlyException("Something went wrong.");
		}
	}

	public WebResponse createDefaultResponse(WebHeader header) {
		if (header == null) {
			header = new WebHeader();
			header.setMessageID(UUID.randomUUID().toString());
		}

		header.setMessageTime(new Date());
		WebResponse response = new WebResponse();
		response.setHeader(header);

		return response;
	}

	public boolean validateRequest() {
		return true;
	}

	public WebResponse createDefaultResponse() {
		return createDefaultResponse(null);
	}

	private WebResponse getErrorResponse(short errorCode, String errorMessage) {
		List<ErrorInfo> errors = new ArrayList<>();
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode(errorCode);
		errorInfo.setErrorMsg(errorMessage);
		errors.add(errorInfo);

		WebResponse response = createDefaultResponse();
		response.setErrors(errors);
		;
		return response;
	}

	public DAOManager getDaoManager() {
		return daoManager;
	}

	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
	}
}
