CREATE DATABASE questionpro;

CREATE TABLE stories (
    id int(5),
    deleted BOOLEAN,
    type varchar(8),
    by_author varchar(32),
    time timestamp(6) NULL,
    text varchar(256),
    dead BOOLEAN,
    parent varchar(265),
    poll int(5),
    url varchar(64),
    score int(5),
    title varchar(256),
    descendants int(5),
    PRIMARY KEY (id)
) ;

CREATE TABLE kids (
    id int(5),
    kid int(5),
    PRIMARY KEY (id, kid)
) ;

CREATE TABLE parts (
    id int(5),
    part int(5),
    PRIMARY KEY (id, part)
) ;
