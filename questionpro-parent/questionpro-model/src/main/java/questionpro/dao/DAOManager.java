package questionpro.dao;

public interface DAOManager {
	
	/**
	 * Initialize the DAO Manager.
	 *
	 */
	void init();
	
	/**
	 * Finalize the DAO Manager. 
	 */
	void destroy();
	
	/**
	 * Get the Story DAO.
	 * 
	 * 
	 * @return
	 */
	StoryDAO getStoryDAO();
}
