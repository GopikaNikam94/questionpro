package questionpro.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JSONUtil {

	private static ObjectMapper mapper = new ObjectMapper();
	
	public static <T> T convertBody(Object body, Class<T> bodyClass) {
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		return mapper.convertValue(body, bodyClass);
	}
	
	public static String getFieldValue(Object json, String field){
		ObjectNode node = null;
		node = mapper.convertValue(json, ObjectNode.class);
		if (node.has(field)) {
			return node.get(field).textValue();
		}
		return null;
	}
}
