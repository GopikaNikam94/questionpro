package questionpro.util;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WebHeader {

	@JsonProperty("rid")
	private String messageID;
	
	@JsonProperty("ts")
	private Date messageTime;
	
	@JsonProperty("channelId")
	private String channelId;

	/**
	 * @return the messageID
	 */
	public String getMessageID() {
		return messageID;
	}

	/**
	 * @param messageID the messageID to set
	 */
	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}

	/**
	 * @return the messageTime
	 */
	public Date getMessageTime() {
		return messageTime;
	}

	/**
	 * @param messageTime the messageTime to set
	 */
	public void setMessageTime(Date messageTime) {
		this.messageTime = messageTime;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
}
