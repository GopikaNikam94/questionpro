package questionpro.util;

public class SQLQuery {
	
	private String query;
	
	public SQLQuery() {}
	
	public SQLQuery(String query) {
		this.query = query;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

}